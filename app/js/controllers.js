'use strict';

/* Controllers */
var moneyManagerControllers = angular.module('moneyManagerControllers', []);

moneyManagerControllers.controller('welcomeController', ['$scope', function($scope){
}]);

moneyManagerControllers.controller('expensesController', ['$scope', 'Expense', function($scope, Expense){
	$scope.allExpenses = Expense.getAll();
}]);