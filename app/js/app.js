'use strict';

/* Application module */
var moneyManagerApp = angular.module('moneyManagerApp', 
	['ngRoute', 'moneyManagerControllers', 'moneyManagerServices']);

moneyManagerApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
	$routeProvider
	.when('/app/welcome',{
		templateUrl: '/app/partials/welcome.html',
        controller: 'welcomeController'
	})
	.when('/app/expenses',{
		templateUrl: '/app/partials/expenses.html',
        controller: 'expensesController'
	})
	.otherwise({
		redirectTo: '/app/welcome'
	});	
	$locationProvider.html5Mode(true);
}]);
