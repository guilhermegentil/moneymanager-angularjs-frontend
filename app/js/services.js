'use strict';

/* Services */
var moneyManagerServices = angular.module('moneyManagerServices', ['ngResource']);

/* Creates a new resource called Expense*/
moneyManagerServices.factory('Expense', ['$resource', function($resource){
	var response = $resource('http://localhost:8080/moneymanager-dev/api/expense', {}, {
		getAll: {method:'GET', params:{}, isArray:true}
	});

	return response;
}]);
